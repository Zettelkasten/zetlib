#version 400 core

in vec2 position;

uniform mat3 transform;

void main(void) {
    gl_Position = vec4(transform * vec3(position, 1.0), 1.0);
}
