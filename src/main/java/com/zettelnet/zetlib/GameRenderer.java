package com.zettelnet.zetlib;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import com.zettelnet.zetlib.input.KeyAdapter;
import com.zettelnet.zetlib.input.KeyCallback;
import com.zettelnet.zetlib.input.MouseAdapter;
import com.zettelnet.zetlib.input.MouseCallback;
import com.zettelnet.zetlib.loop.LoopCallback;
import com.zettelnet.zetlib.loop.LoopInitializer;
import com.zettelnet.zetlib.loop.RenderLoop;
import com.zettelnet.zetlib.opengl.Renderer;
import com.zettelnet.zetlib.util.Color;

public class GameRenderer implements LoopInitializer, LoopCallback {

	private boolean fullscreen = false;
	private boolean resizable = false;
	private int suggestedWidth, suggestedHeight;

	private String windowTitle;
	private Color backgroundColor = new Color(0.5f);

	private final Game game;

	private final RenderLoop loop = new RenderLoop(this, this);
	private final KeyAdapter keyAdapter = new KeyAdapter();
	private final MouseAdapter mouseAdapter = new MouseAdapter();

	private final List<Renderer> renderers = new ArrayList<>();
	private final Deque<Renderer> addRenderers = new ArrayDeque<>();
	private final Deque<Renderer> removeRenderers = new ArrayDeque<>();

	public long window;

	public GameRenderer(final Game game, String windowTitle) {
		this.game = game;
		this.windowTitle = windowTitle;
	}

	public boolean setRunning(boolean running) {
		return loop.setRunning(running);
	}

	public boolean isRunning() {
		return loop.isRunning();
	}

	@Override
	public void init() {
		GLFWErrorCallback.createPrint(System.err).set();

		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		createWindow();

		glClearColor(backgroundColor.r(), backgroundColor.g(), backgroundColor.b(), 0.0F);

		for (Renderer renderer : renderers) {
			renderer.init();
		}
	}

	private void createWindow() {
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, resizable ? GLFW_TRUE : GLFW_FALSE);

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		int windowWidth = fullscreen ? vidmode.width() : (suggestedWidth > 0 ? suggestedWidth : (int) (vidmode.width() * 0.8F));
		int windowHeight = fullscreen ? vidmode.height() : (suggestedHeight > 0 ? suggestedHeight : (int) (vidmode.height() * 0.8F));

		// Create the window
		window = glfwCreateWindow(windowWidth, windowHeight, windowTitle, fullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);
		if (window == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		glfwSetKeyCallback(window, keyAdapter);
		this.mouseAdapter.setWindow(window);
		glfwSetMouseButtonCallback(window, mouseAdapter);

		// Center our window
		glfwSetWindowPos(window, (vidmode.width() - windowWidth) / 2, (vidmode.height() - windowHeight) / 2);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		GL.createCapabilities();
	}

	@Override
	public void destroy() {
		for (Renderer renderer : renderers) {
			renderer.destroy();
		}

		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		glfwTerminate();

		GLFWErrorCallback callback = glfwSetErrorCallback(null);
		if (callback != null) {
			callback.free();
		}
	}

	public void onCycle() {
		if (glfwWindowShouldClose(window)) {
			game.setRunning(false);
		}

		double time = game.getUpdateLoop().getCycleProgress(System.currentTimeMillis());

		glfwPollEvents();

		// clear the framebuffer & swap color buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// remove / add renders
		while (!removeRenderers.isEmpty()) {
			Renderer toRemove = removeRenderers.pop();
			if (renderers.remove(toRemove)) {
				toRemove.destroy();
			}
		}
		while (!addRenderers.isEmpty()) {
			Renderer toAdd = addRenderers.pop();
			if (renderers.add(toAdd)) {
				toAdd.init();
			}
		}

		for (Renderer renderer : renderers) {
			renderer.render(time);
		}

		glfwSwapBuffers(window);
	}

	public void addRenderer(Renderer renderer) {
		this.addRenderers.add(renderer);
	}

	public void removeRenderer(Renderer renderer) {
		this.removeRenderers.add(renderer);
	}

	public void addKeyListener(KeyCallback callback) {
		keyAdapter.registerCallback(callback);
	}

	public void addMouseListener(MouseCallback callback) {
		mouseAdapter.registerCallback(callback);
	}

	public KeyAdapter getKeyAdapter() {
		return keyAdapter;
	}

	public MouseAdapter getMouseAdapter() {
		return mouseAdapter;
	}

	public boolean isFullscreen() {
		return fullscreen;
	}

	public void setFullscreen(boolean fullscreen) {
		if (this.fullscreen == fullscreen) {
			return;
		}

		this.fullscreen = fullscreen;

		if (isRunning()) {
			destroy();
			init();
		}
	}

	public void setTitle(String windowTitle) {
		this.windowTitle = windowTitle;
	}

	public String getTitle() {
		return windowTitle;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public int getWidth() {
		int[] width = new int[1];
		glfwGetWindowSize(window, width, null);
		return width[0];
	}

	public int getHeight() {
		int[] height = new int[1];
		glfwGetWindowSize(window, null, height);
		return height[0];
	}

	public void setSize(int width, int height) {
		this.suggestedWidth = width;
		this.suggestedHeight = height;
	}
}
