package com.zettelnet.zetlib;

import com.zettelnet.zetlib.loop.Loop;
import com.zettelnet.zetlib.loop.UpdateLoop;
import com.zettelnet.zetlib.opengl.ModelManager;
import com.zettelnet.zetlib.opengl.Renderer;

public abstract class AbstractGame implements Game, Renderer {

	private boolean running;

	private final GameRenderer renderer;
	private final ModelManager modelManager = new ModelManager();

	private final Loop updateLoop = new UpdateLoop(null, () -> {
		update(1D);
	}, 20);

	public AbstractGame() {
		this("New game");
	}
		
	public AbstractGame(String windowTitle) {
		this.renderer = new GameRenderer(this, windowTitle);
		
		renderer.addRenderer(modelManager);
		renderer.addRenderer(this);
	}

	public abstract void update(double time);

	public abstract void init();

	public abstract void render(double time);

	public abstract void destroy();
	
	// getters / setters

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public boolean setRunning(boolean running) {
		if (this.running != running) {
			renderer.setRunning(running);
			updateLoop.setRunning(running);
			this.running = running;
			return true;
		} else {
			return false;
		}
	}
	
	public GameRenderer getRenderer() {
		return renderer;
	}
	
	public ModelManager getModelManager() {
		return modelManager;
	}
	
	@Override
	public Loop getUpdateLoop() {
		return updateLoop;
	}
}
