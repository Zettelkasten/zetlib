package com.zettelnet.zetlib.awt;

import java.awt.Graphics2D;

public interface Renderer2D {

	void render(Graphics2D g, double time);
}
