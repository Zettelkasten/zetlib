package com.zettelnet.zetlib.awt;

import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import com.zettelnet.util.vector.Vector;

public class Display2D extends JFrame {

	private static final long serialVersionUID = 0L;

	private final Renderer2D renderer;

	private int width, height;
	private Vector area = Vector.withValues(0, 0);

	public Display2D(final Renderer2D renderer, String title, int width, int height) {
		this.renderer = renderer;

		this.setTitle(title);
		this.setSize(width, height);
		this.setMenuBar(null);

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(true);
	}

	public void doRender() {
		doRender(0);
	}

	public void doRender(double time) {
		Insets insets = getInsets();
		width = getWidth() - insets.left - insets.right;
		height = getHeight() - insets.top - insets.bottom;
		area = Vector.withValues(width, height);

		BufferedImage backBuffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = backBuffer.createGraphics();
		performRender(g, time);
		g.dispose();

		getGraphics().drawImage(backBuffer, insets.left, insets.top, this);
	}

	private void performRender(Graphics2D g, double time) {
		g.clearRect(0, 0, getWidth(), getHeight());

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);

		renderer.render(g, time);
	}

	public BufferedImage drawImage() {
		return drawImage(0);
	}

	public BufferedImage drawImage(double time) {
		BufferedImage buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = buffer.createGraphics();
		performRender(g, 0);
		g.dispose();
		return buffer;
	}

	public int getVisibleWidth() {
		return width;
	}

	public int getVisibleHeight() {
		return height;
	}

	public Vector getArea() {
		return area;
	}
}
