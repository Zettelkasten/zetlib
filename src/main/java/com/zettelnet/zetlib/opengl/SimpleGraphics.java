package com.zettelnet.zetlib.opengl;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL11.*;

import com.zettelnet.zetlib.util.Color;

public class SimpleGraphics implements Renderer {

	private final ModelManager modelManager;

	private RawModel squareModel, squareLinesModel, circleModel, lineModel, pointModel;
	private ShaderProgram shader;
	private int transformUniform, colorUniform;

	public SimpleGraphics(final ModelManager modelManager) {
		this.modelManager = modelManager;
	}

	@Override
	public void init() {
		squareModel = ModelUtils.createSquare(modelManager);
		squareLinesModel = ModelUtils.createSquareLines(modelManager);
		circleModel = ModelUtils.createCircle(modelManager, 20);
		lineModel = ModelUtils.createLine(modelManager);
		pointModel = ModelUtils.createPoint(modelManager);
		shader = new ShaderProgram("/vertex.glsl", "/fragment.glsl") {
			@Override
			protected void bindAttributes() {
				super.bindAttribute(0, "position");
			}
		};
		transformUniform = shader.getUniformLocation("transform");
		colorUniform = shader.getUniformLocation("color");

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		
//		glEnable(GL_LINE_SMOOTH);
//		glEnable(GL_POINT_SMOOTH);
//		glEnable(GL_POLYGON_SMOOTH);
	}

	public void enable() {
		shader.enable();
	}

	public void disable() {
		shader.disable();
	}

	@Override
	public void render(double time) {
	}

	@Override
	public void destroy() {
		shader.destroy();
	}

	private float[] makeTransformationMatrix(float x, float y, float width, float height, float rotation) {
		return Transformation2D.combine(
				Transformation2D.translate(x, y),
				Transformation2D.scale(width, height),
				Transformation2D.rotate(rotation)
				);
	}

	public void transform(float x, float y, float width, float height) {
		transform(makeTransformationMatrix(x, y, width, height, 0f));
	}

	public void transform(float x, float y, float width, float height, float rotation) {
		transform(makeTransformationMatrix(x, y, width, height, rotation));
	}

	private void transform(float[] matrix) {
		glUniformMatrix3fv(transformUniform, false, matrix);
	}

	public void stroke(float lineWidth) {
		glLineWidth(lineWidth);
		glPointSize(lineWidth);
	}

	public void drawRect(float x, float y, float width, float height) {
		drawRect(x, y, width, height, 0f);
	}

	public void drawRect(float x, float y, float width, float height, float rotation) {
		transform(x, y, width, height, rotation);
		modelManager.render(squareLinesModel);
	}

	public void fillRect(float x, float y, float width, float height) {
		fillRect(x, y, width, height, 0f);
	}

	public void fillRect(float x, float y, float width, float height, float rotation) {
		transform(x, y, width, height, rotation);
		modelManager.render(squareModel);
	}

	public void fillOval(float x, float y, float width, float height) {
		fillOval(x, y, width, height, 0f);
	}

	public void fillOval(float x, float y, float width, float height, float rotation) {
		transform(x, y, width, height, rotation);
		modelManager.render(circleModel);
	}

	public void drawLine(float ax, float ay, float bx, float by) {
		float dx = bx - ax;
		float dy = by - ay;
		
		float len = (float) Math.sqrt(dx * dx + dy * dy);
		float angle = (float) Math.asin(dy / len);
		
		float mx = ax + dx / 2;
		float my = ay + dy / 2;
		transform(mx, my, len, len, angle);
		modelManager.render(lineModel);
	}

	public void drawPoint(float x, float y) {
		transform(x, y, 1f, 1f);
		modelManager.render(pointModel);
	}

	public void color(Color color) {
		color(color.r(), color.g(), color.b());
	}

	public void color(float r, float g, float b) {
		glUniform3f(colorUniform, r, g, b);
	}
}
