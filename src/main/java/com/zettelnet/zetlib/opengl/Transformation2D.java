package com.zettelnet.zetlib.opengl;

public class Transformation2D {

	public static float[] translate(float x, float y) {
		return new float[] {
				1, 0, 0,
				0, 1, 0,
				x, y, 1
		};
	}
	
	public static float[] scale(float width, float height) {
		return new float[] {
				width, 0, 0,
				0, height, 0,
				0, 0, 1
		};
	}
	
	public static float[] rotate(float angle) {
		float sin = angle == 0 ? 0 : (float) Math.sin(angle);
		float cos = angle == 0 ? 1 : (float) Math.cos(angle);
		return new float[] {
				cos, sin, 0,
				-sin, cos, 0,
				0, 0, 1
		};
	}
	
	public static float[] combine(float[] first, float[]... matrices) {
		float[] a = first;
		for (float[] b : matrices) {
			float[] c = new float[3 * 3];
			
			c[0] = a[0] * b[0] + a[3] * b[1] + a[6] * b[2];
			c[1] = a[1] * b[0] + a[4] * b[1] + a[7] * b[2];
			c[2] = a[2] * b[0] + a[5] * b[1] + a[8] * b[2];
			c[3] = a[0] * b[3] + a[3] * b[4] + a[6] * b[5];
			c[4] = a[1] * b[3] + a[4] * b[4] + a[7] * b[5];
			c[5] = a[2] * b[3] + a[5] * b[4] + a[8] * b[5];
			c[6] = a[0] * b[6] + a[3] * b[7] + a[6] * b[8];
			c[7] = a[1] * b[6] + a[4] * b[7] + a[7] * b[8];
			c[8] = a[2] * b[6] + a[5] * b[7] + a[8] * b[8];
			
			a = c;
		}
		return a;
	}
}
