package com.zettelnet.zetlib.opengl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glTexImage2D;

import de.matthiasmann.twl.utils.PNGDecoder;
import de.matthiasmann.twl.utils.PNGDecoder.Format;

public class ImageUtils {

	public static void loadPNG(String resourceName) {
		try (InputStream in = ImageUtils.class.getResourceAsStream(resourceName)) {
			PNGDecoder decoder = new PNGDecoder(in);

			int width = decoder.getWidth();
			int height = decoder.getHeight();

			ByteBuffer buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
			decoder.decode(buffer, decoder.getWidth() * 4, Format.RGBA);
			buffer.flip();

			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
		} catch (IOException e) {
			throw new RuntimeException("Failed to load image", e);
		}
	}
}
