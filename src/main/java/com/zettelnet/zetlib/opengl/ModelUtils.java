package com.zettelnet.zetlib.opengl;

import static org.lwjgl.opengl.GL11.*;

public class ModelUtils {

	private static final float[] SQUARE_VERTICES = {
			-0.5f, 0.5f,
			-0.5f, -0.5f,
			0.5f, -0.5f,
			0.5f, 0.5f,
	};

	private static final int[] SQUARE_INDICES = {
			0, 1, 3,
			3, 1, 2
	};

	private static final int[] SQUARE_LINE_INDICES = {
			0, 1, 2, 3
	};

	public static TexturedModel createSprite(ModelManager manager, String textureResource, float textureX, float textureY, float textureWidth, float textureHeight, ShaderProgram... shaders) {
		float[] textureCoords = {
				textureX, textureY,
				textureX, textureY + textureHeight,
				textureX + textureWidth, textureY + textureHeight,
				textureX + textureWidth, textureY
		};
		return manager.createTexturedModel(SQUARE_VERTICES, textureCoords, SQUARE_INDICES, GL_TRIANGLES, textureResource, shaders);
	}

	public static RawModel createSquare(ModelManager manager) {
		return manager.createModel(SQUARE_VERTICES, SQUARE_INDICES, GL_TRIANGLES);
	}

	public static RawModel createSquareLines(ModelManager manager) {
		return manager.createModel(SQUARE_VERTICES, SQUARE_LINE_INDICES, GL_LINE_LOOP);
	}

	public static RawModel createCircle(ModelManager manager, int segments) {
		assert segments >= 3;
		final float radius = 0.5f;

		final float[] VERTICES = new float[(1 + segments) * 2];
		VERTICES[0] = 0f;
		VERTICES[1] = 0f;
		for (int i = 0; i < segments; i++) {
			VERTICES[(i + 1) * 2 + 0] = (float) Math.cos((float) i / segments * Math.PI * 2) * radius;
			VERTICES[(i + 1) * 2 + 1] = (float) Math.sin((float) i / segments * Math.PI * 2) * radius;
		}

		final int[] INDICES = new int[1 + segments + 1];
		for (int i = 0; i < segments + 1; i++) {
			INDICES[i] = i;
		}
		INDICES[segments + 1] = 1;

		return manager.createModel(VERTICES, INDICES, GL_TRIANGLE_FAN);
	}

	public static RawModel createLine(ModelManager manager) {
		return manager.createModel(new float[] { -0.5f, 0f, 0.5f, 0f }, new int[] { 0, 1 }, GL_LINES);
	}
	
	public static RawModel createPoint(ModelManager manager) {
		return manager.createModel(new float[] { 0f, 0f }, new int[] { 0 }, GL_POINTS);
	}
}
