package com.zettelnet.zetlib.opengl;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import com.zettelnet.zetlib.input.KeyAdapter;
import com.zettelnet.zetlib.input.KeyCallback;
import com.zettelnet.zetlib.input.MouseAdapter;
import com.zettelnet.zetlib.input.MouseCallback;
import com.zettelnet.zetlib.loop.LoopCallback;
import com.zettelnet.zetlib.loop.LoopInitializer;
import com.zettelnet.zetlib.loop.RenderLoop;
import com.zettelnet.zetlib.loop.UpdateLoop;

public class WindowRenderer implements LoopInitializer, LoopCallback {

	private final UpdateLoop updateLoop;

	private final String windowTitle;
	private final boolean fullscreen;
	private int windowWidth, windowHeight;

	private final RenderLoop loop = new RenderLoop(this, this);
	private final KeyAdapter keyAdapter = new KeyAdapter();
	private final MouseAdapter mouseAdapter = new MouseAdapter();

	private final List<Renderer> renderers = new ArrayList<>();

	private long window;
	private boolean shouldClose = false;

	public WindowRenderer(final UpdateLoop updateLoop, final String windowTitle, final boolean fullscreen, final int prefferedWidth, final int prefferedHeight) {
		this.updateLoop = updateLoop;
		this.windowTitle = windowTitle;
		this.fullscreen = fullscreen;
		this.windowWidth = prefferedWidth;
		this.windowHeight = prefferedHeight;
	}

	public boolean setRunning(boolean running) {
		return loop.setRunning(running);
	}

	@Override
	public void init() {
		GLFWErrorCallback.createPrint(System.err).set();

		if (!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW");
		}

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

		// Get the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		if (fullscreen) {
			windowWidth = vidmode.width();
			windowHeight = vidmode.height();
		}

		// Create the window
		window = glfwCreateWindow(windowWidth, windowHeight, windowTitle, fullscreen ? glfwGetPrimaryMonitor() : NULL, NULL);
		if (window == NULL) {
			throw new RuntimeException("Failed to create the GLFW window");
		}

		glfwSetKeyCallback(window, keyAdapter);
		this.mouseAdapter.setWindow(window);
		glfwSetMouseButtonCallback(window, mouseAdapter);

		// Center our window
		glfwSetWindowPos(window, (vidmode.width() - windowWidth) / 2, (vidmode.height() - windowHeight) / 2);

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);

		GL.createCapabilities();

		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

		for (Renderer renderer : renderers) {
			renderer.init();
		}
	}

	@Override
	public void destroy() {
		for (Renderer renderer : renderers) {
			renderer.destroy();
		}

		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		glfwTerminate();

		GLFWErrorCallback callback = glfwSetErrorCallback(null);
		if (callback != null) {
			callback.free();
		}
	}

	public void onCycle() {
		if (glfwWindowShouldClose(window)) {
			this.shouldClose = true;
		}

		double time = updateLoop.getCycleProgress(System.currentTimeMillis());

		glfwPollEvents();

		// clear the framebuffer & swap color buffers
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		for (Renderer renderer : renderers) {
			renderer.render(time);
		}

		glfwSwapBuffers(window);
	}

	public boolean shouldClose() {
		return shouldClose;
	}

	public void addRenderer(Renderer renderer) {
		this.renderers.add(renderer);
	}

	public void removeRenderer(Renderer renderer) {
		this.renderers.remove(renderer);
	}

	public void addKeyListener(KeyCallback callback) {
		keyAdapter.registerCallback(callback);
	}

	public void addMouseListener(MouseCallback callback) {
		mouseAdapter.registerCallback(callback);
	}

	public KeyAdapter getKeyAdapter() {
		return keyAdapter;
	}

	public MouseAdapter getMouseAdapter() {
		return mouseAdapter;
	}
}
