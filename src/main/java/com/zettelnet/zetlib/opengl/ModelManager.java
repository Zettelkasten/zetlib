package com.zettelnet.zetlib.opengl;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_INT;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glDrawElements;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_ELEMENT_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glDeleteBuffers;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glUniform1i;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glDeleteVertexArrays;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;

public class ModelManager implements Renderer {

	private final List<Integer> vaos = new ArrayList<>();
	private final List<Integer> vbos = new ArrayList<>();
	private final List<Integer> textures = new ArrayList<>();
	private int usedTextures = 0;

	public RawModel createModel(float[] positions, int[] indices, int drawMode) {
		int vaoID = createVao();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 2, positions);
		unbindVAO();
		return new RawModel(vaoID, indices.length, drawMode);
	}

	public ModelTexture createTexture(String resource, String samplerUniformName, ShaderProgram... shaders) {
		int activeTexture = GL_TEXTURE0 + usedTextures;
		int tex = glGenTextures();
		glActiveTexture(activeTexture);
		glBindTexture(GL_TEXTURE_2D, tex);
		ImageUtils.loadPNG(resource);
		glGenerateMipmap(GL_TEXTURE_2D);

		for (ShaderProgram shader : shaders) {
			shader.enable();
			int samperUniform = shader.getUniformLocation(samplerUniformName);
			glUniform1i(samperUniform, usedTextures);
			shader.disable();
		}

		usedTextures++;
		return new ModelTexture(tex, activeTexture);
	}

	public TexturedModel createTexturedModel(float[] vertices, float[] textureCoords, int[] indices, int drawMode, String textureResourceName, ShaderProgram... shaders) {
		int vaoID = createVao();
		bindIndicesBuffer(indices);
		storeDataInAttributeList(0, 2, vertices);
		storeDataInAttributeList(1, 2, textureCoords);
		unbindVAO();
		RawModel rawModel = new RawModel(vaoID, indices.length, drawMode);
		return new TexturedModel(rawModel, createTexture(textureResourceName, "tex", shaders));
	}

	@Override
	public void init() {
	}

	@Override
	public void render(double time) {
	}

	@Override
	public void destroy() {
		for (int vao : vaos) {
			glDeleteVertexArrays(vao);
		}
		for (int vbo : vbos) {
			glDeleteBuffers(vbo);
		}
		for (int texture : textures) {
			glDeleteTextures(texture);
		}
	}

	private int createVao() {
		int vaoID = glGenVertexArrays();
		vaos.add(vaoID);
		glBindVertexArray(vaoID);
		return vaoID;
	}

	private void storeDataInAttributeList(int attributeNumber, int attributeSize, float[] data) {
		int vboID = glGenBuffers();
		vbos.add(vboID);
		glBindBuffer(GL_ARRAY_BUFFER, vboID);
		FloatBuffer buffer = RenderUtils.createFloatBuffer(data);
		glBufferData(GL_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
		glVertexAttribPointer(attributeNumber, attributeSize, GL_FLOAT, false, 0, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	private void unbindVAO() {
		glBindVertexArray(0);
	}

	private void bindIndicesBuffer(int[] indices) {
		int vboID = glGenBuffers();
		vbos.add(vboID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
		IntBuffer buffer = RenderUtils.createIntBuffer(indices);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, buffer, GL_STATIC_DRAW);
	}

	public void render(RawModel model) {
		glBindVertexArray(model.getVaoID());
		glEnableVertexAttribArray(0);
		glDrawElements(model.getDrawMode(), model.getVertexCount(), GL_UNSIGNED_INT, 0);
		glDisableVertexAttribArray(0);
		glBindVertexArray(0);
	}

	public void render(TexturedModel model) {
		RawModel rawModel = model.getRawModel();
		ModelTexture texture = model.getTexture();

		glBindVertexArray(rawModel.getVaoID());
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glActiveTexture(texture.getActiveTexture());
		glBindTexture(GL_TEXTURE_2D, texture.getTextureId());
		glDrawElements(rawModel.getDrawMode(), rawModel.getVertexCount(), GL_UNSIGNED_INT, 0);
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glBindVertexArray(0);
	}
}
