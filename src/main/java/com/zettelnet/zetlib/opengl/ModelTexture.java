package com.zettelnet.zetlib.opengl;

public class ModelTexture {

	private final int textureId;
	private final int activeTexture;
	
	public ModelTexture(final int textureId, final int activeTexture) {
		this.textureId = textureId;
		this.activeTexture = activeTexture;
	}
	
	public int getTextureId() {
		return textureId;
	}
	
	public int getActiveTexture() {
		return activeTexture;
	}
}
