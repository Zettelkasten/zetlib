package com.zettelnet.zetlib.opengl;

public interface Renderer {

	void init();
	
	void render(double time);
	
	void destroy();
}
