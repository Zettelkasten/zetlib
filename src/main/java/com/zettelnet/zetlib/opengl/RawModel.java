package com.zettelnet.zetlib.opengl;

public class RawModel {

	private int vaoID;
	private int vertexCount;

	private int drawMode;
	
	public RawModel(int vaoID, int vertexCount, int drawMode) {
		this.vaoID = vaoID;
		this.vertexCount = vertexCount;
		this.drawMode = drawMode;
	}

	public int getVaoID() {
		return vaoID;
	}

	public int getVertexCount() {
		return vertexCount;
	}

	public int getDrawMode() {
		return drawMode;
	}
}
