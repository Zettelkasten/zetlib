package com.zettelnet.zetlib;

import com.zettelnet.zetlib.loop.Loop;
import com.zettelnet.zetlib.opengl.ModelManager;

public interface Game {

	boolean isRunning();

	boolean setRunning(boolean running);

	GameRenderer getRenderer();

	ModelManager getModelManager();

	Loop getUpdateLoop();
}
