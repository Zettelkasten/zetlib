package com.zettelnet.zetlib.util;

import java.util.Random;

public class Color {

	private static final Random rnd = new Random();
	
	private final float r, g, b;

	public Color(final float i) {
		this(i, i, i);
	}
	
	public Color(final float r, final float g, final float b) {
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public float r() {
		return r;
	}

	public float g() {
		return g;
	}

	public float b() {
		return b;
	}

	public static Color randomColor() {
		return new Color(rnd.nextFloat(), rnd.nextFloat(), rnd.nextFloat());
	}
}
