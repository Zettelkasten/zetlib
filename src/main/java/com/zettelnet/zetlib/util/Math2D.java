package com.zettelnet.zetlib.util;

import com.zettelnet.util.vector.Vector;

public class Math2D {

	public static double lerp(double a, double b, double time) {
		return a + time * (b - a);
	}

	public static float lerp(float a, float b, double time) {
		return (float) (a + time * (b - a));
	}

	public static Vector lerp(Vector a, Vector b, double time) {
		if (a.size() != b.size()) {
			throw new IllegalArgumentException("Vectors have to have same size");
		}
		double[] vector = new double[Math.max(a.size(), b.size())];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = lerp(a.get(i), b.get(i), time);
		}
		return Vector.withValues(vector);
	}

	public static double constrain(double val, double min, double max) {
		return Math.min(Math.max(val, min), max);
	}

	public static float constrain(float val, float min, float max) {
		return Math.min(Math.max(val, min), max);
	}

	public static int constrain(int val, int min, int max) {
		return Math.min(Math.max(val, min), max);
	}

	public static long constrain(long val, long min, long max) {
		return Math.min(Math.max(val, min), max);
	}
}
