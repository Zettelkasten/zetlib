package com.zettelnet.zetlib.loop;

public class UpdateLoop extends Thread implements Loop {

	private final LoopInitializer initializer;
	private final LoopCallback callback;

	private static final long SECOND = 1000;

	private boolean initialized = false;
	private boolean running = false;
	private boolean stop = false;

	// global

	private double targetFrequency;
	private long targetCycleDuration;

	// current cycle

	private long cycleStartTime;
	private long processFinishTime;
	private long processDuration;
	private long sleepDuration;

	private double delta;

	// last cycle

	private long lastCycleStartTime;
	private long lastCycleDuration;

	public UpdateLoop(LoopInitializer initializer, LoopCallback callback, double frequency) {
		this.initializer = initializer;
		this.callback = callback;

		setTargetFrequency(frequency);
	}

	@Override
	public void run() {
		try {
			if (initializer != null && !initialized) {
				initializer.init();
				this.initialized = true;
			}

			running = true;
			while (running) {
				cycleStartTime = now();
				lastCycleDuration = (this.cycleStartTime - this.lastCycleStartTime);
				delta = (double) targetCycleDuration / lastCycleDuration;

				callback.onCycle();

				processFinishTime = now();
				processDuration = processFinishTime - cycleStartTime;
				sleepDuration = targetCycleDuration - processDuration;

				if (sleepDuration > 0) {
					try {
						Thread.sleep(sleepDuration);
					} catch (InterruptedException e) {
						throw new RuntimeException(e);
					}
				}

				lastCycleStartTime = cycleStartTime;

				if (stop) {
					this.running = false;
				}
			}
		} finally {
			if (initializer != null && initialized) {
				initializer.destroy();
				this.initialized = false;
			}
		}
	}

	private long now() {
		return System.currentTimeMillis();
	}

	@Override
	public boolean setRunning(boolean running) {
		boolean changed = this.running != running;
		if (changed) {
			if (running) {
				start();
			} else {
				synchronized (this) {
					stop = true;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	// global

	@Override
	public synchronized long getTargetCycleDuration() {
		return targetCycleDuration;
	}

	@Override
	public double getTargetFrequency() {
		return targetFrequency;
	}

	public double setTargetFrequency(double frequency) {
		double prev = this.targetFrequency;

		this.targetFrequency = frequency;
		this.targetCycleDuration = (long) (SECOND / frequency);

		return prev;
	}

	// last cycle

	@Override
	public long getLastCycleStartTime() {
		return lastCycleStartTime;
	}

	@Override
	public long getLastCycleDuration() {
		return lastCycleDuration;
	}

	@Override
	public double getLastDelta() {
		return delta;
	}

	// current cycle

	@Override
	public long getCurrentCycleStartTime() {
		return cycleStartTime;
	}

	@Override
	public double getCycleProgress(long time) {
		double progress = (double) (time - this.cycleStartTime) / targetCycleDuration;
		if (progress < 0) {
			progress = 0;
		}
		if (progress > 1) {
			progress = 1;
		}
		return progress;
	}

	@Override
	public double getCurrentFrequency() {
		// TODO Auto-generated method stub
		return 0;
	}
}
