package com.zettelnet.zetlib.loop;

public class RenderLoop extends Thread {

	private final LoopInitializer initializer;
	private final LoopCallback callback;

	private boolean initialized = false;
	private boolean running = false;

	private boolean toStop = false;

	public RenderLoop(LoopInitializer initializer, LoopCallback callback) {
		this.initializer = initializer;
		this.callback = callback;
	}

	@Override
	public void run() {
		try {
			if (initializer != null && !initialized) {
				initializer.init();
				this.initialized = true;
			}

			running = true;
			while (running) {
				try {
					Thread.sleep(1000 / getFrequency());
					callback.onCycle();
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}

				if (toStop) {
					this.running = false;
				}
			}
		} finally {
			if (initializer != null && initialized) {
				initializer.destroy();
				this.initialized = false;
			}
		}
	}

	private void scheduleStop() {
		this.toStop = true;
	}

	public int getFrequency() {
		return 120;
	}

	public boolean setRunning(boolean running) {
		boolean changed = this.running != running;
		if (changed) {
			if (running) {
				start();
			} else {
				scheduleStop();
			}
			return true;
		} else {
			return false;
		}
	}
	
	public boolean isRunning() {
		return running;
	}
}
