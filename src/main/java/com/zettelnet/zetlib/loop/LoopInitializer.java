package com.zettelnet.zetlib.loop;

public interface LoopInitializer {

	void init();
	
	void destroy();
}
