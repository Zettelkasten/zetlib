package com.zettelnet.zetlib.loop;

public interface Loop {

	// general

	boolean setRunning(boolean running);

	boolean isRunning();

	double getTargetFrequency();

	double setTargetFrequency(double frequency);

	long getTargetCycleDuration();

	// current cycle

	long getCurrentCycleStartTime();

	double getCycleProgress(long time);

	double getCurrentFrequency();

	// last cycle

	long getLastCycleStartTime();

	long getLastCycleDuration();

	double getLastDelta();
}
