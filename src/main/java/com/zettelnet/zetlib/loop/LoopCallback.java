package com.zettelnet.zetlib.loop;

public interface LoopCallback {

	void onCycle();
}
