package com.zettelnet.zetlib.update;

public interface Updater {

	void update(double delta);
}
