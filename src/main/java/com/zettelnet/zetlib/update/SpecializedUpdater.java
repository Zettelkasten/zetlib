package com.zettelnet.zetlib.update;

public interface SpecializedUpdater<T> {

	void update(T entity, double delta);
}
