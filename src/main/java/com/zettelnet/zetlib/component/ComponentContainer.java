package com.zettelnet.zetlib.component;

import java.util.Set;

public interface ComponentContainer {

	Set<Component> getComponents();
}
