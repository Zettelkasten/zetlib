package com.zettelnet.zetlib.states;

public class StateManager {

	private final GameState currentState;

	public StateManager(final GameState initState) {
		this.currentState = initState;
	}

	public void init() {
		currentState.init();
	}

	public void update() {
		currentState.update();
	}

	public void destroy() {
		currentState.destroy();
	}

	public GameState getCurrentState() {
		return currentState;
	}

	public void changeState(GameState newState) {
		assert currentState != newState;
		currentState.destroy();
		newState.init();
	}
}
