package com.zettelnet.zetlib.states;

public interface GameState {

	void init();
	
	void update();
	
	void destroy();
}
