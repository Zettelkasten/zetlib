package com.zettelnet.zetlib.input;

@FunctionalInterface
public interface MouseCallback {

	void invoke(long window, double x, double y, int button, int action, int mods);
}
