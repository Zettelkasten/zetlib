package com.zettelnet.zetlib.input;

@FunctionalInterface
public interface KeyCallback {

	void invoke(long window, int key, int scancode, int action, int mods);

}
