package com.zettelnet.zetlib.input;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.glfw.GLFWKeyCallbackI;

public class KeyAdapter implements GLFWKeyCallbackI {

	private final Set<KeyCallback> callbacks = new HashSet<>();

	public void registerCallback(KeyCallback callback) {
		this.callbacks.add(callback);
	}

	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {
		for (KeyCallback controller : callbacks) {
			controller.invoke(window, key, scancode, action, mods);
		}
	}
}
