# README #

ZetLib is a Java game engine for developing small Java AWT 2D or LWJGL games.

## Building a project with LWJGL ##

Include the following to your project's `pom.xml` to build a runnable `.jar` including LWJGL natives:


```
#!xml

<properties>
	<mainClass>your.project.MainClass</mainClass>
</properties>

<repositories>
	<repository>
		<id>zettelnet</id>
		<url>https://bitbucket.org/zettelnet/mvn-repo/raw/default/releases</url>
	</repository>
	<repository>
		<id>zettelnet-snapshots</id>
		<url>https://bitbucket.org/zettelnet/mvn-repo/raw/default/snapshots</url>
		<snapshots>
			<enabled>true</enabled>
		</snapshots>
	</repository>
</repositories>

<dependencies>
	<dependency>
		<groupId>com.zettelnet</groupId>
		<artifactId>zetlib</artifactId>
		<version>0.0.1-SNAPSHOT</version>
	</dependency>
</dependencies>

<build>
	<plugins>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-compiler-plugin</artifactId>
			<version>3.5.1</version>
			<configuration>
				<source>1.8</source>
				<target>1.8</target>
			</configuration>
		</plugin>
		<plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-assembly-plugin</artifactId>
			<executions>
				<execution>
					<phase>package</phase>
					<goals>
						<goal>single</goal>
					</goals>
				</execution>
			</executions>
			<configuration>
				<descriptorRefs>
					<descriptorRef>jar-with-dependencies</descriptorRef>
				</descriptorRefs>
				<archive>
					<manifest>
						<mainClass>${mainClass}</mainClass>
					</manifest>
				</archive>
			</configuration>
		</plugin>
		<plugin>
			<groupId>com.googlecode.mavennatives</groupId>
			<artifactId>maven-nativedependencies-plugin</artifactId>
			<version>0.0.7</version>
			<inherited>true</inherited>
			<executions>
				<execution>
					<id>unpacknatives</id>
					<phase>generate-resources</phase>
					<goals>
						<!--suppress MavenModelInspection (this line is for IDEA) -->
						<goal>copy</goal>
					</goals>
				</execution>
			</executions>
		</plugin>
	</plugins>
</build>
```